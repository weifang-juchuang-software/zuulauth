package com.tianyalei.core.zuulauth.annotation;

/**
 * @author wuweifeng wrote on 2019/8/12.
 */
public enum Logical {
    AND,
    OR;

    private Logical() {
    }
}
