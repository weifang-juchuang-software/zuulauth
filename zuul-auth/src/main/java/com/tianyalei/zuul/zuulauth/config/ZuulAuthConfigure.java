package com.tianyalei.zuul.zuulauth.config;

import com.tianyalei.core.zuulauth.config.AuthConfigure;
import com.tianyalei.core.zuulauth.config.properties.AuthFetchDurationProperties;
import com.tianyalei.core.zuulauth.zuul.AuthInfoHolder;
import com.tianyalei.zuul.zuulauth.gateway.AuthChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.zuul.ZuulProxyMarkerConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;

/**
 * @author wuweifeng wrote on 2019-08-13.
 */
@Configuration
@EnableConfigurationProperties(AuthFetchDurationProperties.class)
@ConditionalOnClass(ZuulProxyMarkerConfiguration.class) //这一句是当前工程是zuul工程时，才启用该configuration
public class ZuulAuthConfigure extends AuthConfigure {

    @Resource
    private ApplicationContext applicationContext;

    @Bean
    @ConditionalOnMissingBean
    AuthChecker authChecker() {
        logger.info("============ ZUUL AuthChecker load ============="+applicationContext);
        return new AuthChecker(applicationContext);
    }
}
