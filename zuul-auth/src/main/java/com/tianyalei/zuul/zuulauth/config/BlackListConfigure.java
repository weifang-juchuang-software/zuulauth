package com.tianyalei.zuul.zuulauth.config;

import com.tianyalei.zuul.zuulauth.gateway.filter.BlackListFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.netflix.zuul.ZuulProxyAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wuweifeng wrote on 2019-08-19.
 */
@Configuration
@ConditionalOnMissingBean(BlackListConfigure.class)
@ConditionalOnClass(ZuulProxyAutoConfiguration.class)
public class BlackListConfigure {
    @Bean
    public BlackListFilter blackListFilter() {
        return new BlackListFilter();
    }


}
